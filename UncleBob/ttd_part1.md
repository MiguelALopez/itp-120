## **What are the Three Laws of Test-driven Development?**
1. write no production code expect to pass a faiing test
2. write only enough of a test to demonstrate a failure
3. write only enough production code to the test 

## **Explain the Red -> Green -> Refactor -> Red process.**
### Write the test first then go little by little. write a little bit of code, test it. If it passes move on if not
### fix it then and there

## **What are the three characteristics Uncle Bob lists of rotting code? (Note: these were covered in Episode 1).** 
### Rigid Fragile and Immobile 

## **Explain how fear promotes code rot and why the fear exists in the first place. How does TDD help us break this vicious cycle?**
### We fear to fix bad so we leave it be only for it rot more and more. And this fear exist from the idea of if you fix one 
### thing you break two other things.If you test the code as you go it minimizes the of going back to fix it once its too late.

## **Uncle Bob mentions FitNesse as an example of a project that uses TDD effectively. What is FitNesse? What does it do?**
### FitNesse is a wiki and aceptance testing framework. It runs test for your code.

## **What does Uncle Bob say about a program with a long bug list? What does he say this comes from?**
### irresponsibility and carelessness from a development team that has been behaving unprofessionally 

## **What two other benefits does Uncle Bob say you get from TDD besides a reduction in debugging time?**
### More effienct and prevents code from rotting in first place
