# **HOW TO ADD WASHINGTON DC TO TRANSPORTR**

___

### 1. Make a fork of https://github.com/grote/transportr.

 Go into build.gradle and find the line with the PTE repo address in build.gradle. 

	implementation('com.gitlab.opentransitmap:public-transport-enabler:XXXXXXX) {

 Note this hash code.

___

### 2. Make a fork of https://gitlab.com/opentransitmap/public-transport-enabler. (PTE)

Find the commit associated with the hashcode from earlier.

Checkout that commit. Make a new branch `washingtondc` for your changes.

Our diffs are listed here:
https://gitlab.com/opentransitmap/public-transport-enabler/-/merge_requests/5/diffs

**Add the NetworkProvider:**

**Add a file in src/de/schildbach/pte called WashingtonDCProvider.java**

**Put the following code in WashingtonDCProvider.java:**

	/*
	 * Copyright 2014-2015 the original author or authors.
	 *
	 * This program is free software: you can redistribute it and/or modify
	 * it under the terms of the GNU General Public License as published by
	 * the Free Software Foundation, either version 3 of the License, or
	 * (at your option) any later version.
	 *
	 * This program is distributed in the hope that it will be useful,
	 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 	 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 	 * GNU General Public License for more details.
 	 *
 	 * You should have received a copy of the GNU General Public License
	 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
	*/

	package de.schildbach.pte;

	import okhttp3.HttpUrl;

	/**
	* @author Your Name
	*/
	public class WashingtonDCProvider extends AbstractNavitiaProvider {
	private static String API_REGION = "us-dc";

    public WashingtonDCProvider(final HttpUrl apiBase, final String authorization) {
    	super(NetworkId.WASHINGTONDC, apiBase, authorization);

		setTimeZone("America/New_York");
    }

		public WashingtonDCProvider(final String authorization) {
		super(NetworkId.WASHINGTONDC, authorization);

		setTimeZone("America/New_York");
    }

    @Override
    public String region() {
    	return API_REGION;
    	}
	}

___

### 3. Make changes to your fork of https://github.com/grote/transportr so that you can use this WashingtonDCNetworkProvider in the Transportr app.

Diffs listed here: https://github.com/liamnorm/Transportr/commit/9868e621899417e71e25783f56e40214e63a8a4a 
They are as follows:

In build.gradle:
Change the hashcode in this line to the hashcode of the commit when your changes were added to the PTE repo.

`implementation('com.gitlab.opentransitmap:public-transport-enabler:XXXXXXXX)`

Add strings to app/src/main/res/values/strings.xml:

	<string name="np_name_usdc">Washington, DC</string>
	<string name="np_desc_usdc" translatable="false">Metro</string>

Add The NetworkProvider to app/src/main/java/de/grobox/transportr/networks/TransportNetworks.kt:
              
	TransportNetwork(
		id = NetworkId.WASHINGTONDC,
		name = R.string.np_name_usdc,
		description = R.string.np_desc_usdc,
		status = ALPHA,
		factory = { WashingtonDCProvider(NAVITIA) }
	)

Run `./update-dependency-pinning.sh`

Build and test the app.

___

### 4. Make a merge request to the Transportr repo.
