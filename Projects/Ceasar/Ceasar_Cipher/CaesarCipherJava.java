import java.util.Scanner;
import java.io.File;
 
public class CaesarCipherJava {
    public static void main(String...s)throws Exception {
        String message, encryptedMessage = "";
        int key;
        char ch;
        
        File file = new File("/home/migoo/itp-120/Projects/Ceasar/Ceasar_Cipher/sample.txt"); 
        Scanner scanner = new Scanner(file);
        StringBuffer sb = new StringBuffer();
        while(scanner.hasNext()) {
            sb.append(" "+scanner.nextLine());
        }
        message = sb.toString();
        
        System.out.println("Enter key: ");
        key = scanner.nextInt();
        for(int i = 0; i < message.length(); ++i){
            ch = message.charAt(i);
            if(ch >= 'a' && ch <= 'z'){
                ch = (char)(ch + key);
                
                if(ch > 'z'){
                    ch = (char)(ch - 'z' + 'a' - 1);
                }
                
                encryptedMessage += ch;
            }
            else if(ch >= 'A' && ch <= 'Z'){
                ch = (char)(ch + key);
                
                if(ch > 'Z'){
                    ch = (char)(ch - 'Z' + 'A' - 1);
                }
                
                encryptedMessage += ch;
            }
            else {
                encryptedMessage += ch;
            }
        }
        System.out.println("Encrypted Message = " + encryptedMessage);
    }
}


